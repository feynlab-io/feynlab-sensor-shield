# FeynLab.io Sensor Shield 

import RPi.GPIO as GPIO

class RelayController:
    """Relay and LED Controller"""

    #Variables
    is_initialized = False

    #Pins
    relay1, relay2 , led1 , led2= 20, 12, 18, 27

    #Initialize
    def __init__(self):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.relay1, GPIO.OUT)
        #GPIO.setup(self.relay2, GPIO.OUT)
	GPIO.setup(self.led1, GPIO.OUT)
	GPIO.setup(self.led2, GPIO.OUT)
        GPIO.output(self.relay1, GPIO.LOW)
        #GPIO.output(self.relay2, GPIO.LOW)
	GPIO.output(self.led1, GPIO.LOW)
        GPIO.output(self.led2, GPIO.LOW)
	self.is_initialized = True
        return

    #Relay Control
    def set_relay(self, ch, st):
        """Controls the relay.
        :param ch: Relay channel. 1
        :param st: Relay state. True of False."""
        if (ch == 1):
            GPIO.output(self.relay1, GPIO.HIGH if st else GPIO.LOW)
        #elif (ch == 2):
        #    GPIO.output(self.relay2, GPIO.HIGH if st else GPIO.LOW)
        return
 
    #LED Control
    def set_led(self, ch, st):
        """Controls the LEDs.
        :param ch: LED channel. 1 or 2
        :param st: LED state. True of False."""
        if (ch == 1):
            GPIO.output(self.led1, GPIO.HIGH if st else GPIO.LOW)
        elif (ch == 2):
            GPIO.output(self.led2, GPIO.HIGH if st else GPIO.LOW)
        return

    #Relay Readout
    def read_relay_state(self, ch):
        """Reads the relay state.
        :param ch: Relay channel. 1."""
        if (ch == 1):
            return GPIO.input(self.relay1)
        #elif (ch == 2):
        #    return GPIO.input(self.relay2)

    #LED Readout
    def read_led_state(self, ch):
        """Reads the LED state.
        :param ch: LED channel. 1 or 2."""
        if (ch == 1):
            return GPIO.input(self.led1)
        elif (ch == 2):
            return GPIO.input(self.led2)

    #Disposal
    def __del__(self):
        """Releases the resources."""
        if self.is_initialized:
            GPIO.output(self.relay1, GPIO.LOW)
            #GPIO.output(self.relay2, GPIO.LOW)
            GPIO.output(self.led1, GPIO.LOW)
	    GPIO.output(self.led2, GPIO.LOW)
	    GPIO.cleanup()
            del self.is_initialized
        return