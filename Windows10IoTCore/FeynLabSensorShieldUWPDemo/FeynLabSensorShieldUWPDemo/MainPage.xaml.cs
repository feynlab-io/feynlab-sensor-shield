﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using FeynLabSensorShield;
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace FeynLabSensorShieldUWPDemo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        static BME280Sensor bme;
        static double slp = 1033.0;

        // Sensor timer
        Timer sensorTimer;

        public MainPage()
        {
            this.InitializeComponent();

            Initialize();
        }

        private async void Initialize()
        {
            // Initialize and configure sensor
            await InitializeBME280();
            // Configure timer to 2000ms delayed start and 2000ms interval
            sensorTimer = new Timer(new TimerCallback(SensorTimerTick), null, 2000, 2000);
        }

        private async Task InitializeBME280()
        {
            bme = new BME280Sensor();

            // Optional advanced sensor configuration
            await bme.SetOversamplingsAndMode(
                BME280Sensor.HumidityOversampling.x04,
                BME280Sensor.TemperatureOversampling.x04,
                BME280Sensor.PressureOversampling.x04,
                BME280Sensor.SensorMode.Normal);

            // Optional advanced sensor configuration
            await bme.SetConfig(
                BME280Sensor.InactiveDuration.ms0500,
                BME280Sensor.FilterCoefficient.fc04);
        }
        private static void SensorTimerTick(object state)
        {
            // Write sensor data to output / immediate window
            Debug.WriteLine("Temperature..: " + bme.ReadTemperature().ToString("00.0") + "C");
            Debug.WriteLine("Humidity.....: %" + bme.ReadHumidity().ToString("00.0" + "RH"));
            Debug.WriteLine("Pressure.....: " + bme.ReadPressure().ToString(".0") + "Pa");
            Debug.WriteLine("Altitude.....: " + bme.ReadAltitude(slp).ToString(".0") + "m");
            Debug.WriteLine("-----");
        }

    }
}
